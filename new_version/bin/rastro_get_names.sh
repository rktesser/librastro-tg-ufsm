#!/bin/sh

# Script que gera de modo pratico os arquivos de saida com as 
#funcoes necessarias utilizadas e passadas para ele


if [ "$CC" == "" ]
then
	CC="gcc"
fi

SRCS=""
NENTRADA=$#
I=1
if [ 2 -gt $NENTRADA ]
then
	echo "ERRO: Parametros invalidos."
	echo "  USO: $0 <arquivos com funcoes...> <arquivo sem terminacao>" 
	echo " PS: Se quiser setar flags, sete a varialvel \"CFLAGS\" "
	exit
fi


while [ $I -lt $NENTRADA ]
do
	SRCS="$SRCS $1"
	shift
	I=$((I+1))
done

if [ -e $1 ]
then
	echo "ERRO: Arquivo $1 ja existente"
	exit
fi

$CC -E $CFLAGS $SRCS -I./include/ 2>/dev/null | ./bin/rastro_names.sh > $1
./bin/rastro_generate $1 $1.c $1.h
rm -f $1
echo "Inclua nos arquivos \"$SRCS\" a linha abaixo."
echo "#include \"$1.h\""
