//#include "rastro_public.h"
//#include "rastro_private.h"
#include "rastro_write_functions.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/param.h>  /* for MAXHOSTNAMELEN */
#include "list.h"

list_t list;
bool list_init=false;

//Mutexes used for syncronization on buffer page switching;
pthread_mutex_t rst_mutex_chgpg= PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rst_mutex_verify_flushed = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rst_mutex_to_flush = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t rst_cond_pg_flushed = PTHREAD_COND_INITIALIZER;
pthread_cond_t rst_cond_to_flush = PTHREAD_COND_INITIALIZER;
pthread_t rst_flusher_thread;

rst_buffer_t *rst_global_buffer;

int rst_end_tracing;

int rst_debug_mask = 0;
char rst_dirname[FILENAME_MAX];


//le o arquivo /proc/cpuinfo para pegar a frequencia do processador, em mhz
double get_freq()
{
	FILE * cpuinfo;
	double freq;
	char campo[80];
	if(!(cpuinfo = fopen("/proc/cpuinfo","r"))){
		perror("Can not open file /proc/cpuinfo\nat function get_freq\n");
		exit(1);
	}

	//laço para encontrar a frequencia
	do{
			fscanf(cpuinfo, "%s", campo);
	}while(strcmp("MHz",campo) != 0);
	//achou a linha
	fscanf(cpuinfo, "%s",campo);//pega o :
	fscanf(cpuinfo, "%lf", &freq);
	fclose(cpuinfo);
//	printf("A frequencia do processador eh: %u MHz\n", freq);
	return freq ;
}

void rst_buffer_destroy(rst_buffer_t *b)
{
	if( b == NULL){
		fprintf(stderr, 
			"[rastro]Erro: Impossível destruir buffer de rastros!\n");
		return;
	}
	if(close(b->rst_fd) == -1){
		fprintf(stderr, "[rastro] erro ao fechar arquivo de rastros!\n");
	};
	free(b->rst_buffer);
	free(b->pages);
	free(b);
	return;
}
/*
void rst_destroy_buffer(void *p)
{
    rst_buffer_t *ptr = (rst_buffer_t *) p;
    
    if (ptr != NULL) {
		int fd;
        rst_event_ptr(ptr, RST_EVENT_STOP);
        rst_flush(ptr);
        free(ptr->rst_buffer);
	fd = RST_FD(ptr);
        close(fd);
        //free(ptr);
    }
}
*/

// Extrai argumentos para a biblioteca
void extract_arguments(int *argcp, char ***argvp)
{
    char **argv;
    int argc;
    int arg, used, unused;
    char **p1, **p2, **plast;
    char *cepar;

    argv = *argvp;
    argc = *argcp;
    
    arg = used = unused = 1;
    bzero(rst_dirname, sizeof rst_dirname);
    while (arg < argc) {
        if ( strncmp(argv[arg], "-rst-", 5) == 0 ) {
            cepar = argv[arg] + 5;
            if ( !strcmp(cepar, "dir") ) {
                strcpy(rst_dirname, argv[++arg]);
                if (opendir(rst_dirname) == NULL ) {
                    printf("Diretorio invalido\n");
                    exit(0);
                }
                if ( (rst_dirname[strlen(rst_dirname) - 1] != '/') )
                    rst_dirname[strlen(rst_dirname)] = '/';
            }
            if ( !strcmp(cepar, "dm") ) {
                rst_debug_mask = atoi(argv[++arg]);
            }
            unused = ++arg;
        }
        else {
            if (unused > used ) {
                p1 = argv + used;
                p2 = argv + unused;
                plast = argv + argc;
                argc -= (unused - used);
                while ( p2 < plast )
                    *p1++ = *p2++;
                unused = used;
            }
            arg = ++used;
        }
    }
    if ( unused > used)
        argc = used;
    *argcp = argc;
    argv[argc] = NULL;
}

/*
 * This function allocates memory for the buffer's an his pages' descriptors
 * and also for the data to be stored in it.
 */
rst_buffer_t * rst_buffer_alloc(rst_buffer_t * b, int page_size, int n_pages)
{
	int erro =0;
	
	if(!(b = (rst_buffer_t *) malloc(sizeof(rst_buffer_t)))){
		erro = 1;
	}else if(!(b->rst_buffer = (char*) malloc(page_size * n_pages))){
		erro = 2;
	}else if(!(b->pages =(rst_page_t *)malloc((sizeof(rst_page_t)) * n_pages))){
		erro = 3;
	}
	if(erro){
		fprintf(stderr,"Erro! Impossível Alocar Memória para o Buffer de Rastros.(%d)\n", erro);
	}
	return b;
}


// Inicializa biblioteca em um nodo
void rst_initialize(u_int64_t id1, u_int64_t id2, int page_size, int n_pages, int *argc, char ***argv)
{
    if ( argv != NULL ) 
		extract_arguments(argc, argv); 
    
	rst_init(id1, id2, page_size, n_pages);
}

// Inicializa a biblioteca em uma thread
void rst_init(u_int64_t id1, u_int64_t id2, int page_size, int n_pages)
{
	rst_buffer_t *ptr;
//	ptr = (rst_buffer_t *) malloc(sizeof(rst_buffer_t));
    ptr = rst_buffer_alloc(ptr, page_size, n_pages);
	if(!ptr){
		fprintf(stderr, "[libRastro] Error allocating memory for the buffer.\n");
		return;
    }
    rst_init_ptr(ptr, id1, id2, page_size, n_pages);
}


// Grava buffer no arquivo de traco
/*
void rst_flush(rst_buffer_t * ptr)
{
    size_t nbytes;
    size_t n;

    nbytes = RST_BUF_COUNT(ptr);
    n = write(RST_FD(ptr), RST_BUF_DATA(ptr), nbytes);
    if (n != nbytes) {
        fprintf(stderr, "[rastro] error writing rastro file\n");
    }
    RST_RESET(ptr);
}
*/
//Writes a buffer page into the trace file.
void rst_flush_page(rst_buffer_t *ptr, int page_index)
{
	size_t n_bytes;
	size_t n;
	char *data;
	n_bytes = ptr->pages[page_index].size;
	data = ptr->pages[page_index].beginning;
	n = write(RST_FD(ptr), data, n_bytes);
	if (n != n_bytes) {
		fprintf(stderr, "[rastro] error writing rastro file\n");
	}
//	fprintf(stdout, "Page %d writen. Size = %d\n",page_index, n);
//	fflush(stdout);
	ptr->pages[page_index].size = 0;
}

void * rst_page_flusher(void * arg)
{
	int current, done;
	rst_page_t * pg;
	rst_buffer_t *ptr;
	ptr = RST_PTR;
	pg = ptr->pages;
	current = 0;
	done = 0;
	while(!done){
		DEBUG_MSG(flusher: lock to_flush);
//		fprintf(stdout,"page to_flush = %d\n",pg[current].to_flush);
//		fflush(stdout);
		pthread_mutex_lock(&rst_mutex_to_flush);
		while(pg[current].to_flush != 1){
			if(!rst_end_tracing){
			DEBUG_MSG(flusher: wait_cond to_flush);
			pthread_cond_wait(&rst_cond_to_flush, 
					&rst_mutex_to_flush);
			}else{
				DEBUG_MSG(flusher: ending tracing);
				done = 1;
				break;
			}
		}
		pthread_mutex_unlock(&rst_mutex_to_flush);
		DEBUG_MSG(flusher : unlock to_flush);
		if(!done){
			while(pg[current].w_ctr != 0){
				usleep(1);
			}
			DEBUG_MSG(flusher : no_writings to be done in this page);

			rst_flush_page(ptr, current);
			DEBUG_MSG(flusher : lock verify_flushed);
			pthread_mutex_lock(&rst_mutex_verify_flushed);
//			fprintf(stdout,"pager: to_flush = %d\n",pg[current].to_flush);
//			DEBUG_MSG(tinha que ser to_flush = 1);
			RST_ATOM_DEC(pg[current].to_flush);
//			fprintf(stdout,"pager: to_flush = %d\n",pg[current].to_flush);
//			DEBUG_MSG(tinha que ser to_flush = 0);
			DEBUG_MSG(flusher : signal  pg_flushed);
			pthread_cond_signal(&rst_cond_pg_flushed);
			pthread_mutex_unlock(&rst_mutex_verify_flushed);
			DEBUG_MSG(flusher : unlock verify_flushed);
//			fprintf(stdout, "flushed page %d\n", current);
//			fflush(stdout);
			current = pg[current].next;
//			fprintf(stdout,"new page %d - to_flush = %d\n ", current, pg[current].to_flush);
//			fflush(stdout);
			DEBUG_MSG(flusher: switched page);
		}
	}
	DEBUG_MSG(flusher: thead exit);
	pthread_exit((void *) NULL);
}
// Inicializacao com buffer pre-alocado
void rst_init_ptr(rst_buffer_t *ptr, u_int64_t id1, u_int64_t id2, int page_size, int n_pages)
{
    int fd, i, rc;
    char fname[30];
    char hostname[MAXHOSTNAMELEN+1];
    pthread_attr_t attr;

   // size_t mystacksize;
    
    pthread_attr_init(&attr);
   // pthread_attr_getstacksize (&attr, &mystacksize);
    //fprintf(stdout, "Stack size = %d\n", mystacksize);
//    fflush(stdout);

    if ( ptr == NULL ) {
        fprintf(stderr, "[rastro] error inicializing - invalid pointer\n");
        return;
    }
    
    if(!list_init){
		list_initialize(&list, list_copy, list_cmp, list_destroy);
	list_init=true;
    }
    
    RST_SET_PTR(ptr);

//   ptr->rst_buffer_size = 100000;
//   ptr->rst_buffer = malloc(ptr->rst_buffer_size);
//   RST_RESET(ptr);
    
    //Initialize the page data.
    for(i=0; i < n_pages; i++)
    {
		ptr->pages[i].beginning =  (char *)(ptr->rst_buffer + i * page_size);
		ptr->pages[i].next = (i + 1) % n_pages;//It's a circular buffer.
		ptr->pages[i].limit = (char *) (ptr->rst_buffer + (i + 1) * page_size);
		ptr->pages[i].size = 0;
		ptr->pages[i].w_ctr = 0; 
		ptr->pages[i].to_flush = 0; 
    }
    ptr->n_pages = n_pages;
    ptr->page_size = page_size;
    ptr->rst_buffer_ptr = ptr->rst_buffer;
    ptr->current = 0;
    ptr->pgchg_ctr = 0;
    sprintf(fname, "rastro-%lld-%lld.rst",/* dirname,*/ id1, id2);
    fd = open(fname, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fd == -1) {
        fprintf(stderr, "[rastro] cannot open file %s: %s\n", 
                        fname, strerror(errno));
        return;
    }
    
    list_insert_after(&list, NULL, (void *)ptr );
    
    RST_SET_FD(ptr, fd);
    
    rst_end_tracing = 0;
    gethostname(hostname, sizeof(hostname));

    //Initilize the thead responsible for writing data to the file.
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    rc = pthread_create(&rst_flusher_thread, &attr, rst_page_flusher, NULL);
    pthread_attr_destroy(&attr);
    if(rc){
		fprintf(stderr, "[rastro] Error creating thread!\npthread_create() returned %d\n", rc);
		return;
    }
    //Now we register an inicialization event.
    XCAT(rst_event_,LETRA_UINT64,LETRA_STRING,LETRA_DOUBLE,_ptr)(ptr, RST_EVENT_INIT, id1, id2, hostname, get_freq());
}


// Termina biblioteca em nodo ou thread
void rst_finalize(void)
{
    int rc;

    rst_buffer_t *ptr = RST_PTR;

    DEBUG_MSG(finalize: lock chgpg);
    pthread_mutex_lock(&rst_mutex_chgpg);
//		ptr->current = ptr->pages[ptr->current].next;
		ptr->pages[ptr->current].size = ptr->rst_buffer_ptr - ptr->pages[ptr->current].beginning;
		DEBUG_MSG(finalize: lock to_flush);
		pthread_mutex_lock(&rst_mutex_to_flush);
		RST_ATOM_INC(ptr->pages[ptr->current].to_flush);
		DEBUG_MSG(finalize: signal to_flush);
		pthread_cond_signal(&rst_cond_to_flush);
		pthread_mutex_unlock(&rst_mutex_to_flush);
		DEBUG_MSG(finalize: unlock to_flush);
	DEBUG_MSG(finalize: lock verify_flushed);
	pthread_mutex_lock(&rst_mutex_verify_flushed);
		while(ptr->pages[ptr->current].to_flush != 0){
			DEBUG_MSG(finalize: wait cond pg_flushed);
			pthread_cond_wait(&rst_cond_pg_flushed, &rst_mutex_verify_flushed);
		}
	pthread_mutex_unlock(&rst_mutex_verify_flushed);
	DEBUG_MSG(finalize: unlock verify_flushed);
	DEBUG_MSG(finalize: lock to_flush);
	pthread_mutex_lock(&rst_mutex_to_flush);
			rst_end_tracing = 1;
		DEBUG_MSG(finalize: signal to_flush);
		pthread_cond_signal(&rst_cond_to_flush);
	pthread_mutex_unlock(&rst_mutex_to_flush);
	DEBUG_MSG(finalize: unlock to_flush);
    pthread_mutex_unlock(&rst_mutex_chgpg);
    DEBUG_MSG(finalize: unlock chgpg);
    rc  = pthread_join(rst_flusher_thread, NULL);
    if(rc){
		fprintf(stderr, "[libRastro] Error joining thread!\n");
    }
    list_remove(&list, (void *)ptr);
    rst_buffer_destroy(ptr);
//    rst_destroy_buffer(ptr);

//    free(ptr);
}

// Termina biblioteca em nodo ou thread com ptr
void rst_finalize_ptr(rst_buffer_t *ptr)
{
    list_remove(&list, (void *)ptr);
//    rst_destroy_buffer(ptr);
    rst_buffer_destroy(ptr);
}
/*
void rst_flush_all(void)
{
    position_t pos;
    rst_buffer_t *ptr;
    for(pos=list.sent->next; pos != list.sent; pos=pos->next ){
		ptr= (rst_buffer_t *)pos->data;
		rst_flush(ptr);
    }
    //list_finalize(&list);
}
*/
// Registra evento somente com tipo
void rst_event(u_int16_t type)
{
    rst_buffer_t *ptr = RST_PTR;
    char * pos;
    int size = RST_SIZE_HEADER + RST_SIZE_TIMESTAMP + RST_SIZE_THREADID;
    size = ALIGN_SIZE(size);

/*...2 para finalizar esse rastro que so tem o tipo...*/
    pos =rst_startevent(ptr, type << 18 | 0x20000, size);
    rst_endevent(ptr, pos);
}

// Registra evento somente com tipo com ptr
void rst_event_ptr(rst_buffer_t *ptr, u_int16_t type)
{
    char * pos;
    int size = RST_SIZE_HEADER + RST_SIZE_TIMESTAMP + RST_SIZE_THREADID;
    size = ALIGN_SIZE(size);
    if ( ptr == NULL ) {
       printf("[rastro] ptr invalido\n");
       return;
    }
/*...2 para finalizar esse rastro que so tem o tipo...*/
    pos = rst_startevent(ptr, type << 18 | 0x20000, size);
    rst_endevent(ptr, pos);
}
