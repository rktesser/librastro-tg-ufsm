/* Do not edit. File generated by rastro_generate. */

#include "rastro_write_functions.h"

void rst_event_llsd_ptr(rst_buffer_t *ptr, u_int16_t type, u_int64_t l0, u_int64_t l1, u_int8_t *s0, double d0)
{
	char * rst_pos;
	int size;
	size = 41;
	size += strlen((char *) s0);
	size = ALIGN_SIZE(size);
	rst_pos = rst_startevent(ptr, type<<18|0x22441, size);
	RST_PUT(rst_pos, double, d0);
	RST_PUT(rst_pos, u_int64_t, l0);
	RST_PUT(rst_pos, u_int64_t, l1);
	RST_PUT_STR(rst_pos, s0);
	rst_pos = ALIGN_PTR(rst_pos);
	rst_endevent(ptr, rst_pos);
}

