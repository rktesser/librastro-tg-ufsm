#include "rastro_private.h"
#include "rastro_public.h"

/*
 * This function allocates memory for the buffer's an his pages' descriptors
 * and also for the data to be stored in it.
 */
int rst_buffer_alloc(rst_buffer_t * b, int page_size, int n_pages)
{
	int erro =0;
	
	if(!(b = (rst_buffer_t *) malloc(sizeof(rst_buffer_t)))){
		erro = 1;
	}else if(!(b->rst_buffer = (char*) malloc(page_size * n_pages))){
		erro = 1;
	}else if(!b->pages =(rst_page_t *)
			(malloc(sizeof(rst_page_t) * n_pages))){
		erro = 1;
	}
	if(erro){
		fprintf(stderr,"Erro! Impossível Alocar Memória para o Buffer de Rastros.\n");
		return 0;
	}
	return 1;
}

void rst_buffer_destroy(rst_buffer_t *b)
{
	if( b == NULL){
		fprintf(stderr, 
			"Erro: Impossível destruir buffer de rastros!\n");
		return;
	}
	free(b->rst_buffer);
	free(b->pages);
	free(b);
	return;
}

/*
 * This is the buffer initialization function.
*/
int rst_buffer_init(rst_buffer_t * b, int page_size, int n_pages, FILE *fd)
{
	int i;
	
	//Allocate memory for the buffer.
	if(!rst_buffer_alloc(b, page_size, n_pages)){
		return 0;
	}
	
	//Initialize the page data.
	for(i=0; i < n_pages; i++)
	{
		b->pages[i].beginning = b->data + i * page_size;
		b->pages[i].next = (i + 1) % n_pages;//It's a circular buffer.
		b->pages[i].limit = i + page_size;
		b->pages[i].size = 0;
		b->pages[i].w_ctr = 0; 
	}
	b->rst_fd = fd;
	b->n_pages = n_pages;
	b->page_size = page_size;
	b->rst_buffer_ptr = b->rst_buffer;
	b->current = 0;
	return 1;
}

