#include "rastro_public.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
	rst_file_t data;
	rst_event_t ev;
        int i;
        for (i=2; i<argc; i++) {
  	   if (rst_open_file(argv[i], &data, argv[1], 100000) == -1) {
		fprintf(stderr, "could not open rastro file %s\n", argv[i]);
		continue;
	   }
	}
	while (rst_decode_event(&data, &ev)){
		rst_print_event(&ev);
	}
	rst_close_file(&data);
	return 0;
}

