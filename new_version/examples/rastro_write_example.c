#include "saida.h"

int main(int argc, char *argv[])
{
	rst_init(10, 20);
	rst_event(1);
	rst_event_lws(2, 1, 2, "3");
	rst_event_wlsfcd(3, 1, 2, "3", 4, '5', 6);
	rst_event_iwlsifcd(4, 1, 2, 3, "4", 5, 6, '7', 8);
	rst_finalize();
	return 0;
}
