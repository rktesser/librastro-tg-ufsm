#ifndef _RASTRO_PRIVATE_H_
#define _RASTRO_PRIVATE_H_

#include <sys/time.h>
#include <stdio.h>
#include "rastro_public.h"
#include "rastro_alloc_and_time.h"


#define RST_SIZE_DOUBLE		sizeof(double)
#define RST_SIZE_LONG		8
#define RST_SIZE_FLOAT 		sizeof(float)
#define RST_SIZE_INT		4
#define RST_SIZE_SHORT		2
#define RST_SIZE_CHAR		1
#define RST_SIZE_TIMESTAMP 	8
#define RST_SIZE_HEADER		4
#define RST_SIZE_THREADID	4

#define ALIGN_PTR(p) ((void *)(((int)(p)+3)&(-4)))
#define ALIGN_SIZE(s) (((int)(s)+3)&(-4))

#define RST_MAX_EVENT_SIZE 1000

#define RST_RESET(ptr) (ptr->rst_buffer_ptr = ptr->rst_buffer)
#define RST_FD(ptr) (ptr->rst_fd)
#define RST_SET_FD(ptr, fd) (ptr->rst_fd = fd)
#define RST_T0(ptr) (ptr->rst_t0)
#define RST_SET_T0(ptr, t) (ptr->rst_t0 = t)
#define RST_BUF_COUNT(ptr) (ptr->rst_buffer_ptr - ptr->rst_buffer)
#define RST_BUF_DATA(ptr) (ptr->rst_buffer)
#define RST_BUF_SIZE(ptr) (ptr->rst_buffer_size)

extern rst_buffer_t *rst_global_buffer;

extern pthread_mutex_t rst_mutex_chgpg;
extern pthread_mutex_t rst_mutex_verify_flushed;
extern pthread_mutex_t rst_mutex_to_flush;
extern pthread_cond_t rst_cond_pg_flushed;
extern pthread_cond_t rst_cond_to_flush;
extern pthread_t rst_flusher_thread;


/*Code for debugging.*/
#ifdef RST_DEBUG
#define DEBUG_MSG(msg)\
	do{\
		double freq = 1800; \
		long long time; \
		RST_GET_TIME(time, freq); \
		fprintf(stdout,"[debug] ts: %lld -  Thread: %d - " # msg "\n", time, (int) pthread_self()); \
		fflush(stdout);\
	}while(0)
#else
#define DEBUG_MSG(msg) 
#endif

#define RST_PTR (rst_global_buffer)
#define RST_SET_PTR(ptr) (rst_global_buffer = ptr)

#define RST_THREADID ((u_int32_t)pthread_self())

#define RST_PUT(pos, type, val)						\
	do {								\
		type *p = (type *)pos;					\
		*p++ = (type)(val);					\
		pos = (char *)p;					\
	} while (0)
#define RST_PUT_STR(pos, str) 						\
	do {                  						\
		char *__s1 = pos;					\
		char *__s2 = str;					\
		while ((*__s1++ = *__s2++) != '\0') 			\
			;						\
		pos = __s1;						\
	} while(0)

//#define RST_GET(ptr, type) (*((type *)(ptr))++)
#define RST_GET(ptr, type) ((ptr += sizeof(type)),			\
			    (*(type *)(ptr - sizeof(type))))

#define RST_GET_STR(ptr, str)						\
    do {								\
        char *__s1 = ptr;						\
        char *__s2 = str;						\
        while ((*__s2++ = *__s1++) != '\0')				\
            ;                               				\
	ptr = __s1;							\
    } while(0)


#define RST_CHGPG(pt, pg_end)\
	__asm__ volatile (\
		"movl %5, %%ebx\n\t"\
		"0:\n\t"\
		"movl %3, %%eax\n\t"\
		"lock\n\t"\
		"cmpxchg %%ebx, %0\n\t"\
		"jnz 0b\n\t"\
		"movl %4, %1\n\t"\
		"movl %%eax, %2\n\t"\
		:"=m" (pt->rst_buffer_ptr), "=g"(pt->current), "=g" (pg_end)\
		: "m" (pt->rst_buffer_ptr), "g" (pt->pages[pt->current].next), "g"(pt->pages[pt->pages[pt->current].next].beginning)\
		:"eax","ebx","cc", "0"\
	)

//#endif

#define RST_NO_TYPE     0
#define RST_STRING_TYPE 1
#define RST_DOUBLE_TYPE 2
#define RST_FLOAT_TYPE  3
#define RST_LONG_TYPE   4
#define RST_SHORT_TYPE  5
#define RST_CHAR_TYPE   6
#define RST_INT_TYPE    7
#define RST_TYPE_CTR    7


#define RST_INIT    0x10000
#define RST_LAST        0x2

#define RST_BITS_PER_FIELD   4
#define RST_FIELD_MASK       0xf
#define RST_FIELDS_IN_FIRST  4
#define RST_FIELDS_IN_OTHERS 7


void rst_flush(rst_buffer_t * ptr);

// Termina um evento
static inline void rst_endevent(rst_buffer_t * ptr, char * pos)
{
	rst_page_t * pg;
	int page_index = (pos - ptr->rst_buffer -1) / ptr->page_size;
	pg = &ptr->pages[page_index];
	RST_ATOM_DEC(pg->w_ctr);
}

// Inicia um evento
static inline char * rst_startevent(rst_buffer_t * ptr, u_int32_t header, 
		int size)
{
	char * pos, * pg_end;
	u_int32_t threadid;
	int pg_full, rst_end_alloc, rst_current_pg, pgchg_ctr;
do{
	pg_full = 0;
	rst_end_alloc = 0;
	pgchg_ctr = ptr->pgchg_ctr;
	rst_current_pg = ptr->current;
	RST_ATOM_INC(ptr->pages[rst_current_pg].w_ctr);
	RST_ALLOC_AND_TIME(ptr, pos, size, pg_full, rst_current_pg);
	if(pg_full == 1){
		DEBUG_MSG(Buffer page is full);
		RST_ATOM_DEC(ptr->pages[rst_current_pg].w_ctr);
		DEBUG_MSG(lock mutex_chgpg);
		pthread_mutex_lock(&rst_mutex_chgpg);
		if(rst_current_pg == ptr->current && !ptr->pages[ptr->current].to_flush){
			DEBUG_MSG(changing buffer page);
//			ptr->current = ptr->pages[ptr->current].next;
//			ptr->rst_buffer_ptr = ptr->pages[ptr->current].beginning;
			DEBUG_MSG(lock verify_flushed);
			pthread_mutex_lock(&rst_mutex_verify_flushed);
			while(ptr->pages[ptr->pages[rst_current_pg].next].to_flush == 1){
				DEBUG_MSG(cond_wait pg_flushed);
				pthread_cond_wait (&rst_cond_pg_flushed,
						&rst_mutex_verify_flushed);
			}
			DEBUG_MSG(unlock verify_flushed);
			pthread_mutex_unlock(&rst_mutex_verify_flushed);
			RST_CHGPG(ptr, pg_end);
			ptr->pages[rst_current_pg].size = pg_end - ptr->pages[rst_current_pg].beginning;
			DEBUG_MSG(lock to_flush);
			pthread_mutex_lock(&rst_mutex_to_flush);
			RST_ATOM_INC(ptr->pages[rst_current_pg].to_flush);
			DEBUG_MSG(signal to_flush);
			pthread_cond_signal(&rst_cond_to_flush);
			pthread_mutex_unlock(&rst_mutex_to_flush);
			DEBUG_MSG(unlock to_flush);
		}
		pthread_mutex_unlock(&rst_mutex_chgpg);
		DEBUG_MSG(unlock mutex_chgpg);
	}else if(pg_full == 2){
		DEBUG_MSG(address before beginning of current pg);
		RST_ATOM_DEC(ptr->pages[rst_current_pg].w_ctr);
	}else{
		rst_end_alloc = 1;
	}
} while (!rst_end_alloc);
	
	RST_PUT(pos, u_int32_t, header);
//	fprintf(stdout, "TYPE = %x\n",  (*(((int *)pos) -1))>>18&0x3fff);
//	fprintf(stdout, "TSC = %016llx\n",  *(unsigned long long *)pos);
	pos += RST_SIZE_TIMESTAMP; //skip the space occupied by the timestamp
	threadid = (u_int32_t) pthread_self();
	RST_PUT(pos, u_int32_t, threadid);
//	fprintf(stdout,"THREAD= %08x\n", *(((int *)pos) -1));
//	fflush(stdout);
	return pos;
}

#endif /*    _RASTRO_PRIVATE_H_   */
