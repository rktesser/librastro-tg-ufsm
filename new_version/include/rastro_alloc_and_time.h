#ifndef _ALLOC_AND_TIME_H_
#define _ALLOC_AND_TIME_H_

#include <sys/types.h>

#define RST_GET_ROOM(pt,pos,size)\
	__asm__ volatile (\
		"0:\n\t"\
		"movl %2, %%eax\n\t"\
		"movl %2, %%ebx\n\t"\
		"addl %3, %%ebx\n\t"\
		"lock\n\t"\
		"cmpxchg %%ebx, %0\n\t"\
		"jnz 0b\n\t"\
		"movl %%eax, %1\n\t"\
		:"=m" (pt), "=g"(pos)\
		: "m" (pt), "rm" (size) \
		:"eax","ebx","cc", "0"\
	)


//This macro reads the CPU timestamp counter register and calculates the time
//according to the processor clock frequency.
#define RST_GET_TIME(time,freq)\
	do{\
	long double __freq__ = (long double) freq; \
	unsigned  __time_high__=0, __time_low__=0;\
	long double __thd__, __tld__,__td__; \
	__asm__ volatile ("rdtsc\n\t"\
		"movl %%eax, %0\n\t"\
		"movl %%edx, %1\n\t"\
		:"=r" (__time_low__) , "=r" (__time_high__) \
		: \
		:"eax","edx"\
	); \
	__thd__ = ((long double) __time_high__) * 4294967296.0; \
	__tld__ = ((long double) __time_low__) ; \
	__td__ = (__thd__ + __tld__); \
	time = (long long) (__td__ / ((long double) __freq__));\
	} while(0)


#define __USEC (1000000)

//macro wich calculate the time in seconds
#define RST_TSEC(t) ((t) / (__USEC))

//How many seconds passed since the last second has been counted?
#define RST_TUSEC(t) ((t) % (__USEC))

#define RST_TIMERDIFF(t1, t2) ((t1) - (t2))


#define RST_ATOM_INC(val)\
	__asm__ volatile(\
		"lock\n\t"\
		"incl %0\n\t"\
		:"=m" (val)\
		:"m" (val)\
		:"0","1" \
		)

#define RST_ATOM_DEC(val)\
	__asm__ volatile(\
		"lock\n\t"\
		"decl %0\n\t"\
		:"=m" (val)\
		:"m" (val)\
		:"0", "1"\
		)

/*
 *This macro alocates space in the buffer and writes the timestamp into it.
 *The addres of the beginning of the allocated space is stored in "pos".
 *If there is no space left in the current page it changes the 
 *value of the flag wicj indicates that the page is full.
 */
#define RST_ALLOC_AND_TIME(pt,pos,size, __pg_full__, _rst_current_pg)\
do{\
	__asm__ volatile (\
		"movl %3, %%eax\n\t"\
		"0:\n\t"\
		"cmp %%eax, %6\n\t"\
		"jg 1f\n\t"\
		"movl %%eax, %%ebx\n\t"\
		"addl %4, %%ebx\n\t"\
		"cmp %5, %%ebx\n\t"\
		"jge 2f\n\t"\
		"lock\n\t"\
		"cmpxchg %%ebx, %0\n\t"\
		"jnz 0b\n\t"\
		"movl %%eax, %%ebx\n\t"\
		"rdtsc\n\t"\
		"movl %%eax, 4(%%ebx)\n\t"\
		"movl %%edx, 8(%%ebx)\n\t"\
		"movl %%ebx, %1\n\t"\
		"jmp 3f\n\t"\
		"1:\n\t"\
		"movl $2, %2\n\t"\
		"jmp 3f\n\t"\
		"2:\n\t"\
		"movl $1, %2\n\t"\
		"3:\n\t"\
		:"=m" (pt->rst_buffer_ptr), "=g"(pos), "=g"(__pg_full__)\
		:"m" (pt->rst_buffer_ptr), "g"(size),"g" (pt->pages[_rst_current_pg].limit), "g" (pt->pages[_rst_current_pg].beginning) \
		:"cc", "eax", "ebx", "edx","0"\
	);\
}while (0)

double get_freq();
#endif
