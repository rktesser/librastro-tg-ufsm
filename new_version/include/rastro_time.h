#ifndef _RST_TIME_H_
#define _RST_TIME_H_

//This code reads the CPU timestamp counter register and calculates the time
//according to the processor clock frequency.
#define RST_GET_TIME(time,freq)\
	do{\
	double __freq__ = (double) freq; \
	unsigned  __time_high__=0, __time_low__=0;\
	double __thd__, __tld__,__td__; \
	__asm__ volatile ("rdtsc\n\t"\
		"movl %%eax, %0\n\t"\
		"movl %%edx, %1\n\t"\
		:"=r" (__time_low__) , "=r" (__time_high__) \
		: \
		:"eax","edx"\
	); \
	__thd__ = ((double) __time_high__) * 4294967296.0; \
	__tld__ = ((double) __time_low__) ; \
	__td__ = (__thd__ + __tld__); \
	time = (long long) (__td__ / ((double) __freq__));\
	} while(0)


#define MSSEC (1000000)

//macro wich calculate the time in seconds
#define RST_TSEC(t) ((t) / (MSSEC))

//How many seconds passed since the last second has been counted?
#define RST_TMUSEC(t) ((t) % (MSEC))

#define RST_TIMERDIFF(t1, t2) ((t1) - (t2))

double get_freq();
#endif
