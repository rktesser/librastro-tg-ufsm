#include <stdio.h>

#ifdef L1
#include "rastrol1.h"
#endif
#ifdef L2
#include "rastrol2.h"
#endif

#ifdef T1
void gera_eventos()
{
//	printf("T1\n");
	rst_event(1);		
	return;
}
#endif

#ifdef T2
void gera_eventos()
{
	
//	printf("T2\n");
	rst_event_ids(1,1,6.66,"Qualquer coisa!");		
	return;
}
#endif

#ifdef MISTO
void gera_eventos()
{
//	printf("MISTO\n");
	rst_event(1);		
	rst_event_ids(1,1,6.66,"Qualquer coisa!");		
	return;
}
#endif

#ifdef NINST
void gera_eventos()
{
//	printf("NISNT\n");
	return;
}
#endif
