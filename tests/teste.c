#include<stdio.h>
#include<math.h>
#include<sys/time.h>
#include<stdlib.h>

#ifdef COMTHREADS
#include <pthread.h>
#endif

#ifdef L1
#include "rastrol1.h"
#endif

#ifdef L2
#include "rastrol2.h"
#endif


#define N_PAGES 1
#define PAGE_SIZE 1024

#define N_RASTRO 500000
#define N_OPER	20

void gera_eventos();

long long timediff(struct timeval * t0, struct timeval * t1)
{
	struct timeval td;
	long long tdiff;

	td.tv_usec = t1->tv_usec - t0->tv_usec;
	td.tv_sec = t1->tv_sec - t0->tv_sec;
	
	tdiff = (long long)td.tv_sec * 1000000;
	tdiff += (long long)td.tv_usec;
	
	return tdiff;
}

void * faz_algo(void  * arg)
{
	int i, j;
	double res = 0;
#ifdef	L1
#ifdef COMTHREADS
//	printf("L1\n");
	rst_init(0, *(int*) arg);
#endif
#endif
	for(i = 0; i < N_RASTRO; i ++)
	{
		gera_eventos();
		for(j=0; j < N_OPER; j++){
			res += sin(i);
		}
	}
	return (void *) NULL;
}

int main(int argc, char **argv)
{
	int i;
	int id1, id2;
	int n_threads;
#ifdef COMTHREADS
//	printf("com threads\n");
	int rc;
	int *arg;
        pthread_t * thread;
        pthread_attr_t attr;
#endif
	struct timeval t0, tf;
	if(argc != 4){
		fprintf(stdout,"Número de argumentos errado!\n");
		return 1;
	}
	id1 = atoi(argv[1]);
	id2 = atoi(argv[2]);
	n_threads = atoi(argv[3]);
	gettimeofday(&t0, NULL);
#ifdef COMTHREADS
	thread = malloc(sizeof(pthread_t) * n_threads);
	arg = malloc(sizeof(int) * n_threads);
#endif
	gettimeofday(&t0, NULL);
#ifdef	L2
//	printf("L2\n");
	rst_init(id1,id2, PAGE_SIZE, n_threads);
#endif
	
#ifdef COMTHREADS
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for(i = 0; i < n_threads; i++)
	{
		arg[i]=i;
		rc = pthread_create(&thread[i], &attr, faz_algo, (void *) &arg[i]);
		if(rc){
			fprintf(stderr, "Erro ao criar thread!\n");
		}
	}

	pthread_attr_destroy(&attr);

	for(i = 0; i < n_threads; i++)
	{
		rc = pthread_join(thread[i], NULL);

		if(rc){
			fprintf(stderr, "Erro executar join!\n");
		}
	}
#else
//	printf("sem threads\n");
#ifdef	L1
//		printf("L1\n");
		rst_init(id1,id2);
#endif
	for(i = 0; i < n_threads; i++)
	{
		faz_algo((void * ) NULL);
	}
#endif
#ifdef	L2 
	rst_finalize();
#endif
	gettimeofday(&tf, NULL);
#ifdef COMTHREADS
	free(thread);
	free(arg);
#endif
	fprintf(stdout, "%lld\n", timediff(&t0, &tf));
	return 0;
}
