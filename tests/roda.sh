#!/bin/bash
ntimes=10;
iterorthreads=20;

progs=`ls ./bin|grep 'ninst'`
for p in $progs; do 
	timearq=./times/$p.txt;	
	if [ -f $timearq ]; then
		rm $timearq;
	fi
	for i in `seq 1 $ntimes`; do
		echo "$i: ./bin/$p 1 $i $iterorthreads"
		./bin/$p 1 $i $iterorthreads >> $timearq;
	done;
done;

progs=`ls ./bin|grep -v 'ninst'`
for p in $progs; do 
	timearq=./times/$p.txt;	
	if [ -f $timearq ]; then
		rm $timearq;
	fi
	for i in `seq 1 $ntimes`; do
		echo "$i: ./bin/$p 1 $i $iterorthreads"
		./bin/$p 1 $i $iterorthreads >> $timearq;
		rm *.rst;
	done;

done;
